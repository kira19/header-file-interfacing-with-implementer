//This is the header file
//It contains the class specification

#ifndef MAXIMUM_H
#define MAXIMUM_H

class Maximum
{
private:
    int result;
public:
    void set(int, int, int);
    int get() const;
};

#endif // MAXIMUM_H
